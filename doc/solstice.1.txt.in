// Copyright (C) 2016-2018 CNRS, 2018-2019 |Meso|Star>
//
// This is free documentation: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This manual is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
:toc:

solstice(1)
===========

NAME
----
solstice - compute the power collected by a concentrated solar plant

SYNOPSIS
--------
[verse]
*solstice*
*solstice* [_option_]... [_file_]
*solstice* *-g* <__sub-option__[:...]> [_option_]... [_file_]
*solstice* *-p* <__sub-option__[:...]> [_option_]... [_file_]
*solstice* *-r* <__sub-option__[:...]> [_option_]... [_file_]

DESCRIPTION
-----------
*solstice* computes the total power collected by a concentrated solar plant, as
described in the *solstice-input*(5) _file_. If the _file_ argument is not
provided, the solar plant is read from standard input. To evaluate various
efficiencies for each primary reflector, it computes losses due to cosine
effect, shadowing and masking, orientation and surface irregularities,
materials properties and atmospheric extinction. The efficiency for each
one of these effects is subsequently computed for each reflector.

The entities on which computations must be performed are listed in the
*solstice-receiver*(5) file submitted through the *-R* option. The estimated
results follow the *solstice-output*(5) format and are written to the _output_
file or to the standard output whether the *-o* _output_ option is defined or
not, respectively. Note that the *solstice* algorithm is based on the
Monte-Carlo method, which means that every result is provided with its
numerical accuracy.

*solstice* is designed to efficiently handle complex solar facilities: several
reflectors can be specified (planes, conics, cylindro-parabolic, etc.) and
positioned in 3D space, with a possibility for 1-axis and 2-axis
auto-orientation. Multiple materials can be used, as long as the relevant
physical properties are provided. Spectral effects are also taken into account:
it is possible to define the spectral distribution of any physical property,
including the input solar spectrum and the transmissivity of the atmosphere, at
any spectral resolution. Refer to *solstice-input*(5) for more informations.

In addition of the aforementioned computations, *solstice* provides three
other functionalities. The *-g* option can be used to convert the
*solstice-input*(5) geometries in CAO files. The *-p* option saves the sampled
radiative paths used by the estimates, allowing to visualise them externally
which may be a great help to identify a design issue. Finally, the *-r* option
is used to render an image of the submitted solar facility. Note that these
three options are mutually exclusives, and once defined, they replace the
default *solstice* behaviour.

Please note that any coordinate-related question in Solstice must be
considered with the right-handed convention in mind.

OPTIONS
-------
*-D* <__alpha__,__beta__[:...]>::
  List of sun directions. A direction is defined by two angles in degrees. The
  first one, here refered to as _alpha_, is an azimuthal angle in [0, 360[ and
  the second one, here refered to as _beta_, is an elevation in [0, 90].
  Each provided sun direction triggers a new computation whose results are
  concatenated to the _output_ file.
+
Following the right-handed convention, Solstice azimuthal rotation is
counter-clockwise, with 0° on the X axis. Solstice elevation starts from 0° for
directions in the XY plane, up to 90° at zenith. Thus -D0,0 -D90,0 -D180,0 and
-D270,0 will produce solar vectors {-1,0,0} {0,-1,0} {+1,0,0} and {0,+1,0}
respectively, while -D__alpha__,90 will produce {0,0,-1} regardless of _alpha_
value.

*-f*::
  Force overwrite of the output files, i.e. the _output_ file and the file
  where the state of the random number generator is saved (see the *-G*
  option).

*-G* <__sub-option__:...>::
  Save and restore the state of the random number generator. This option can be
  used to ensure the statistical independence between successive simulations
  on the same system. For instance, one can run a new simulation and
  initialising its random number generator with the final state of the
  generator as defined by the previous run. Available sub options are:

  **istate=**_input_rng_state_;;
    Define the file from which the initial state of the random number generator
    is read. If not defined, the random number generator is initialised with
    its default seed.

  **ostate=**_output_rng_state_;;
    Define the file where the final state of the random number generator is
    written. If not defined, this state is simply discarded.
  
*-g* <__sub-option__:...>::
  Generate the shape of the geometry defined in the submitted _file_ and store
  it in _output_. Available sub-options are:

  *format=obj*;;
    Define the file format in which the meshes are stored. Currently, only the
    Alias Wavefront OBJ file format is supported.

  *split=*<**geometry**|*object*|*none*>;;
    Define how the output mesh is split in sub meshes. A sub mesh can be
    generated for each *geometry* or for each *object* as defined in the
    *solstice-input*(5) file format. The *none* option means that only one
    mesh is generated for the whole solar facility. By default, the *split*
    option is set to *none*.

*-h*::
  List short help and exit.

*-n* _experiments-count_::
  Number of Monte-Carlo experiments used to estimate the solar flux. By
  default _experiments-count_ is set to @SOLSTICE_ARGS_DEFAULT_NREALISATIONS@.

*-o* _output_::
  Write results to _output_ with respect to the *solstice-output*(5) format. If
  not defined, write results to standard output.

*-p* <__sub-option__:...>::
  Register the sampled radiative paths for each sun direction and write them to
  _output_. Available sub-options are:

  *default*;;
    Use default sub-options.

  **irlen=**_length_;;
    Length of the radiative path segments going to the infinity. By default, it
    is computed relatively to the scene size.

  **srlen=**_length_;;
    Length of the radiative path segments coming from the sun. By default, it
    is computed relatively to the scene size.

*-q*::
  Do not print the helper message when no _file_ is submitted.

*-R* _receivers_::
  *solstice-receiver*(5) file defining the scene receivers, i.e. the solar
  plant entities for which *solstice* computes Monte-Carlo estimates.

*-r* <__sub-option__:...>::
  Render an image of the scene through a pinhole camera, for each submitted
  sun direction. Write the resulting images to _output_. Available sub-options
  are:

  **fov=**_angle_;;
    Horizontal field of view of the camera in [30, 120] degrees. By default
    _angle_ is @SOLSTICE_ARGS_DEFAULT_CAMERA_FOV@ degrees.

  **img=**_width_**x**_height_;;
    Definition of the rendered image in pixels. By default the image definition
    is @SOLSTICE_ARGS_DEFAULT_IMG_WIDTH@x@SOLSTICE_ARGS_DEFAULT_IMG_HEIGHT@.

  **pos=**_x_**,**_y_**,**_z_;;
    Position of the camera. By default it is set to
    {@SOLSTICE_ARGS_DEFAULT_CAMERA_POS@} or it is automatically computed to
    ensure that the whole scene is visible, whether *tgt* is set or not,
    respectively.

  **rmode=**<**draft**|**pt**>;;
    Rendering mode. In *draft* mode, images are computed by ray-casting; all
    materials are lambertian, the sun is ignored and the only light source is
    positioned at the camera position. In *pt* mode, the scene is rendered with
    the un-biased path-tracing Monte-Carlo algorithm; the materials described
    in the committed _file_ as well as the submitted sun directions are
    correctly handled and an uniform skydome is added to simulate the diffuse
    infinite lighting. By default *rmode* is set to *draft*.

  **spp=**_samples-count_;;
    Number of samples per pixel. If *rmode* is *draft*, the samples position
    into a pixel are the same for all pixels. With *rmode=pt* the pixel
    samples are generated independently for each pixel. By default, use 1
    sample per pixel.

  **tgt=**_x_**,**_y_**,**_z_;;
    Position targeted by the camera. By default, it is set to
    {@SOLSTICE_ARGS_DEFAULT_CAMERA_TGT@} or it is automatically computed to
    ensure that the whole scene is visible, whether *pos* is set or not,
    respectively.

  **up=**_x_**,**_y_**,**_z_;;
    Up vector of the camera. If *rmode* is *pt*, this vector also defines the
    direction toward the top of the skydome. By default, *up* is set to
    {@SOLSTICE_ARGS_DEFAULT_CAMERA_UP@}.

*-t* _threads-count_::
  Hint on the number of threads to use. By default use as many threads as CPU
  cores.

*-v*::
  Make solstice more verbose.

*--version*::
  Output version information and exit.

EXAMPLES
--------

Launch two simulations for sun directions whose azimuthal and elevation angles
are {*45*,*70*} and {*50*,*75*}. The solar facility is described in
*input.yaml* and the receivers on which the integrations must be performed are
declared in the *rcvs.yaml* file. *10000* experiments are used by the
Monte-Carlo estimates and the results are written to *output* even though this
file already exists:

    $ solstice -D45,70:50,75 -R rcvs.yaml -n 10000 -f -o output input.yaml

Generate a mesh for each geometry described in *input.yaml*, and save them in
the *output* file with respect to the Alias Wavefront OBJ format. The meshes
are positioned according to their orientation constraints, with respect to the
sun direction whose azimuthal and elevation angles are {*30*,*60*}. Use the
*csplit*(1) Unix command to generate an Alias Wavefront OBJ file per geometry
stored in *output*. The name of the generated Alias Wavefront OBJ files are
*geom*<__NUM__>**.obj** with __NUM__ in [0, N-1] where N is the number of
geometries described in *input.yaml*. Refer to *solstice-output*(5) for
informations on the regular expression *^---$* used to split the output file:

    $ solstice -D30,60 -g format=obj:split=geometry -f -o output input.yaml
    $ csplit -f geom -b %02d.obj -z --suppress-matched output /^---$/ {*}

Trace 100 radiative paths into the solar plant described in *input.yaml*, with
respect to the sun direction whose azimuthal and elevations angles are *0* and
*90* degrees, respectively. Write the *solstice-output*(5) result to the
standard output and postprocess it with the *sed*(1) Unix command to remove the
first line that stores the sun direction from which the radiative paths come
from. The remaining data that list the radiative paths geometry are redirected
into the *paths.vtk* file:

    $ solstice -n 100 -D0,90 -R rcvs.yaml -p default input.yaml | sed '1d'>paths.vtk

Use the path-tracing rendering algorithm to draw the solar plant
*solplant.yaml* with respect to the sun direction whose azimuthal and elevation
angles are *180* and *45* degrees, respectively. Use *64* samples per pixel to
estimate the per-pixel radiance and fix the up camera vector to {*0*,*0*,*1*}.
Write the *solstice-output*(5) result to standard output and use the *sed*(1)
Unix command to remove the first line which stores the sun direction used to
draw the image. Finally, visualise the rendered picture by redirecting the
remaining data to the *feh*(1) image viewer.

    $ solstice -D180,45 -r up=0,0,1:rmode=pt:spp=64 solplant.yaml | sed '1d' | feh -

COPYRIGHT
---------
Copyright &copy; 2016-2018 CNRS, 2018-2019 |Meso|Star>. License GPLv3+: GNU GPL
version 3 or later <http://gnu.org/licenses/gpl.html>. This is free software.
You are free to change and redistribute it. There is NO WARRANTY, to the extent
permitted by law.

SEE ALSO
--------
*csplit*(1),
*feh*(1),
*sed*(1),
*solstice-input*(5),
*solstice-output*(5),
*solstice-receiver*(5)
